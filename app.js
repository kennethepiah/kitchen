const express = require('express');
const router = require('./server/routes');
const swaggerUi = require('swagger-ui-express');
const dotenv = require('dotenv');
const cors = require('cors');
const swaggerJSDoc = require('swagger-jsdoc');
dotenv.config();

const port = process.env.PORT || 8080;

const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Kitchen API",
            version: "v1",
            description: "A small RESTful API for a Restaurant Management Product called Kitchen"
        },
        servers: [
            {
                url: `https://biznexmans-kitchen-app-9f1658018afc.herokuapp.com/v1/api`
            },
            {
                url: `http://localhost:${port}/v1/api`
            },
        ],
    },
    // This option is the one you're supposed to use
    defaultModelsExpandDepth: -1, 
    // Tried this one also
    defaultModelExpandDepth: -1,
    apis: ["./server/routes/*.js"],
    // Ended up using this option because previous ones didn't work
    customCss: '.models {display: none !important}',
    //customCssUrl: 'https://gitlab.com/kennethepiah/kitchen/-/raw/main/swagger-style.css',
}

const specs = swaggerJSDoc(options);
const app = express();

app.use("/documentation", swaggerUi.serve, swaggerUi.setup(specs));
app.disable('x-powered-by');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/v1', router);
 
app.use('*', (req, res) => {
    res.status(200).json({ message: 'Welcome to the Kitchen API'});
});

app.listen(
    port, 
    () => {console.log(`Service now Running on Port(s): ${port}`)
});