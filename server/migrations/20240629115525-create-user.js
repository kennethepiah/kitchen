'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('KtUsers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      addressLine1: {
        type: Sequelize.STRING
      },
      addressLine2: {
        type: Sequelize.STRING
      },
      addressLine3: {
        type: Sequelize.STRING
      },
      country: {
        type: Sequelize.STRING
      },
      isVerified: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      rating: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      otp: {
        type: Sequelize.STRING
      },
      otpIsUsed: {
        type: Sequelize.STRING
      },
      otpExpireAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
    await queryInterface.sequelize.query(`CREATE UNIQUE INDEX users_email ON Users (email)`);
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('KtUsers');
  }
};