class UserInactiveException extends Error {
    constructor(message) {
      super(message);
      this.message = message;
    }
}
UserInactiveException.prototype.name = 'UserInactiveException';

module.exports = UserInactiveException;