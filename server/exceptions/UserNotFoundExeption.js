class UserNotFoundException extends Error {
    constructor(message) {
      super(message);
      this.message = message;
    }
}
UserNotFoundException.prototype.name = 'UserNotFoundException';

module.exports = UserNotFoundException;