class UserAuthenticationException extends Error {
    constructor(message) {
      super(message);
      this.message = message;
    }
}
UserAuthenticationException.prototype.name = 'UserAuthenticationException';

module.exports = UserAuthenticationException;