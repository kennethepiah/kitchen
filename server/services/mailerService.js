const transporter = require('../config/mailer');

const send = async (to, subject, html) => {

    const mailOptions = {
        from: "Kitchen APP <noreply@kitchen.com>",
        to: to,
        subject: subject,
        html: html,
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
        console.error("Error sending email: ", error);
        return false;
        } else {
        console.log("Email sent: ", info.response);
        return true;
        }
    });
}

module.exports = {send};
