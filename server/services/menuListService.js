const dotenv = require('dotenv');
const apiResponse = require('../utils/apiResponse');
const models = require('../models');
const { MenuItem } = models;
const { validationResult } = require('express-validator');
const { Sequelize, QueryTypes } = require('sequelize');
const atob = require('atob');
dotenv.config();

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mysql'
})


async function search(req, res) {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    let query = `SELECT name, description, cost, quantityDefinition, quantityDefinitionCount, foodCategory, rating, imageName, imageUrl FROM MenuItems where 1=1`;
    if(req.query.id && req.query.id != ''){ query += ` and id = '${req.query.id}'`}
    if(req.query.user && req.query.user != ''){ query += ` and user = '${req.query.user}'`}
    if(req.query.name && req.query.name != ''){ query += ` and name like '%${req.query.name}%'`}
    if(req.query.description && req.query.description != ''){ query += ` and description like '%${req.query.description}%'`}
    if(req.query.costMinimum && req.query.costMinimum != ''){ query += ` and cost >= '${req.query.costMinimum}'`}
    if(req.query.costMaximum && req.query.costMaximum != ''){ query += ` and cost <= '${req.query.costMaximum}'`}
    if(req.query.foodCategory && req.query.foodCategory != ''){ query += ` and foodCategory like '%${req.query.foodCategory}%'`}
    if(req.query.createdAtMinimum && req.query.createdAtMinimum != ''){ query += ` and createdAt >= '${req.query.createdAtMinimum}'`}
    if(req.query.createdAtMaximum && req.query.createdAtMaximum != ''){ query += ` and createdAt <= '${req.query.createdAtMaximum}'`}

    try{ 
        const menuItems = await sequelize.query(query, {
            type: QueryTypes.SELECT,
        });

        if (!menuItems) { return apiResponse(res, 401, `Item not Found`); }
        return apiResponse(res, 200, `Success`,{menuItems});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `There seems to be a problem Fetching your Menu Item. Please Try Again Later`);
    };
}

async function read(req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }
    const claims = JSON.parse(atob(req.headers.authtoken.split('.')[1]));
    try{ 
        const menuItems =
        await MenuItem.findAll(
        { 
            where: { 
                user: claims.id
            }, 
        });

        console.log(menuItems)

        if (!menuItems) { return apiResponse(res, 401, `Item not Found`); }
        return apiResponse(res, 200, `Success`,{menuItems: menuItems});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `There seems to be a problem Fetching your Menu Item. Please Try Again Later`);
    };
}

module.exports = {
    read,
    search
};