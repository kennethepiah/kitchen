const dotenv = require('dotenv');
const apiResponse = require('../utils/apiResponse');
const models = require('../models');
const { MenuItem, Review } = models;
const { validationResult } = require('express-validator');
const atob = require('atob');
// const fileUploadService = require('./fileUploadService');
// const multer = require('multer');
//const upload = multer();

dotenv.config();

async function create(req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    // console.log(req.body)
    // console.log(req.files)
    // process.exit();
    const claims = JSON.parse(atob(req.headers.authtoken.split('.')[1]));
    try{
        const menuItem = await MenuItem.create(
            { 
                user: claims.id,
                name: req.body.name,
                description: req.body.description,
                cost: req.body.cost,
                quantityDefinition: req.body.quantityDefinition,
                quantityDefinitionCount: req.body.quantityDefinitionCount,
                foodCategory: req.body.foodCategory,
                imageName: req.files.imageFile[0].originalname,
                imageUrl: req.files.imageFile[0].location
            });

        const createdMenuItemObj = {
            name: menuItem.name,
            description: menuItem.description,
            cost: menuItem.cost,
            quantityDefinition: menuItem.quantityDefinition,
            quantityDefinitionCount: menuItem.quantityDefinitionCount,
            foodCategory: menuItem.foodCategory,
            imageName: menuItem.imageName
        }

        return apiResponse(res, 200, `Authentication Success`,{menuItem: createdMenuItemObj});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `There seems to be a problem Creating your Menu Listing. Please Try Again Later`);
    };
}

async function read(req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    try{
        const [menuItems] =
        await MenuItem.findAll(
        { 
            where: { 
                id: req.query.id
            }, 
        });

        const reviews =
        await Review.findAll(
        { 
            where: { 
                reviewee: req.query.id,
                type: 'menuitem'
            }, 
        });

        if (!menuItems) { return apiResponse(res, 401, `Item not Found`); }
        menuItems.dataValues.reviews = reviews;
        
        return apiResponse(res, 200, `Success`,{menuItems: menuItems.dataValues});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `There seems to be a problem Fetching your Menu Item. Please Try Again Later`);
    };
}

async function update(req, res) {
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    try{
        const [menuItems] =
        await MenuItem.update(
            { 
                name: req.body.name,
                description: req.body.description,
                cost: req.body.cost,
                quantityDefinition: req.body.quantityDefinition,
                quantityDefinitionCount: req.body.quantityDefinitionCount,
                foodCategory: req.body.foodCategory,
                imageUrl: req.body.imageUrl
            },
            { where: 
                {
                    id: req.query.id
                }, 
            }
        );

        return apiResponse(res, 200, `Success`);
    }
    catch(e){
        console.log(e);
        
    };
}

async function destroy(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    try{
        await MenuItem.destroy(
            { where: 
                {
                    id: req.query.id
                }, 
            }
        );
        return apiResponse(res, 200, `Success`);
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `There seems to be a problem Deleting your Menu Item. Please Try Again Later`);
    };
}

module.exports = {
    create,
    read,
    update,
    destroy
};