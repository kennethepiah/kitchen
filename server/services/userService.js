const jwt = require('jsonwebtoken');
const apiResponse = require('../utils/apiResponse');
const db = require('../models');
const { KtUser } = db;
const { validationResult } = require('express-validator');
const otp = require('../utils/otp');
const mailerService = require('./mailerService')
const atob = require('atob');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
dotenv.config();

async function create(req, res) {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }
    const newotp = await otp.generate();

    const password = await bcrypt.hash(req.body.password, 10);
    try{

        const user =
        await KtUser.create(
            { 
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                phone: req.body.phone,
                email: req.body.email,  
                password: password,
                addressLine1: req.body.addressLine1,
                addressLine2: req.body.addressLine2,
                addressLine3: req.body.addressLine3,
                country: req.body.country,
                isVerified: 0,
                status: 0,
                type: 'customer',
                otp: newotp.otp,
                otpIsUsed: 0,
                otpExpireAt: newotp.expire,
            }, 
        ).catch(function (e) {
            if (e.name == 'SequelizeUniqueConstraintError') {
                const err = new Error(`A User with Email (${req.body.email}) already exists. Email must be unique`);
                err.name = 'SequelizeUniqueConstraintError';
                throw err;
            }
            console.log(e);
            const err = new Error(e.original);
            err.name = 'SequelizeUnknownError';
            throw err;
        });
        
        const createdUserObj = {
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            email: user.email,
            addressline1: user.addressline1,
            addressline2: user.addressline2,
            addressline3: user.addressline3,
            country: user.country,
        }

        mailerService.send(
            req.body.email, 
            'Your Registration @ Kitchen', 
            `<html>
            <h2 style="color: #2e6c80;">
                Hi we are excited to&nbsp; have you <img src="https://html-online.com/editorfiles/tiny/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" />!</h2>
                <p>You have just few steps to complete your registration at Kitchen.&nbsp;</p>
                <p>Click to <a href="https://www.google.com.ng/" target="_blank"> <span style="background-color: #2b2301; color: #fff; display: inline-block; padding: 3px 10px; font-weight: bold; border-radius: 5px;">Here</span></a> to verify your account</p>
                <p><h3><strong><span style="color: #000080;">Use OTP: ${newotp.otp}</span></strong></h3>(*****Expires in 10 Minutes******)</p>
            </h2>
            </html>`
        );

        return apiResponse(res, 200, `success`,{"user": createdUserObj});
    }
    catch(e){
        if (e.name == 'SequelizeUniqueConstraintError') {
            return apiResponse(res, 400, `A User with Email: ${req.body.email} already exists. Email must be unique`);
        }
        return apiResponse(res, 500, "Server appears to be having a hard time, please try again later");
    };
}

async function read(req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    const claims = JSON.parse(atob(req.headers.authtoken.split('.')[1]));
    try{
        const user =
        await KtUser.findAll(
            { 
                where: { 
                    email: claims.email
                },
            }, 
        )
        if (!user) { return apiResponse(res, 401, `User not Found`); }
        return apiResponse(res, 200, `success`,{"user": user});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `Server appears to be having a hard time, please try again later`);
    };
}

async function update(req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    const claims = JSON.parse(atob(req.headers.authtoken.split('.')[1]));
    try{
        await KtUser.update(
            { 
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                phone: req.body.phone,
                addressLine1: req.body.addressLine1,
                addressLine2: req.body.addressLine2,
                addressLine3: req.body.addressLine3,
                country: req.body.country,
            },
            { 
                where: { 
                    email: claims.email
                }, 
            }, 
        )

        const [user] =
        await KtUser.findAll(
            { 
                where: { 
                    email: req.query.email
                }, 
            }, 
        )
        if (!user) { return apiResponse(res, 401, `User not Found`); }
        return apiResponse(res, 200, `success`,{"user": user});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `Server appears to be having a hard time, please try again later`);
    };
}

async function destroy(req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    const claims = JSON.parse(atob(req.headers.authtoken.split('.')[1]));
    try{
        await KtUser.destroy(
            { 
                where: { 
                    email: claims.email
                }, 
            }, 
        )
        
        return apiResponse(res, 200, `success`);
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `Server appears to be having a hard time, please try again later`);
    };
}

module.exports = {
    create,
    read,
    update,
    destroy
};