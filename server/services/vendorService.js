const dotenv = require('dotenv');
const apiResponse = require('../utils/apiResponse');
const models = require('../models');
const { MenuItem , Review} = models;
const { validationResult } = require('express-validator');
const { Op } = require('sequelize');
const { Sequelize, QueryTypes } = require('sequelize');
dotenv.config();

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mysql'
})

async function search(req, res) {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    let query = `SELECT id, concat(firstName," ",lastName) name, concat(addressline1," ",addressline2," ",addressline3," ",country) location FROM Users where type = 'vendor' and isVerified = 1 and status = 1`;
    if(req.query.id && req.query.id != ''){ query += ` and id = '${req.query.id}'`}
    if(req.query.firstName && req.query.firstName != ''){ query += ` and firstName like '%${req.query.firstName}%'`}
    if(req.query.addressline1 && req.query.addressline1 != ''){ query += ` and addressLine1 like '%${req.query.addressline1}%'`}
    if(req.query.addressline2 && req.query.addressline2 != ''){ query += ` and addressLine2 like '%${req.query.addressline2}%'`}
    if(req.query.addressline3 && req.query.addressline3 != ''){ query += ` and addressLine3 like '%${req.query.addressline3}%'`}
    if(req.query.country && req.query.country != ''){ query += ` and country like '%${req.query.country}%'`}

    try{
        const vendor = await sequelize.query(query, {
            type: QueryTypes.SELECT,
        });

        if (!vendor) { return apiResponse(res, 401, `Vendor not Found`); }
        return apiResponse(res, 200, `Success`,{vendor});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `There seems to be a problem Fetching your Menu Item. Please Try Again Later`);
    };
}

async function detail(req, res) {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }

    let query = `SELECT concat(firstName," ",lastName) name, concat(addressline1," ",addressline2," ",addressline3," ",country) location, rating FROM Users where type = 'vendor' and isVerified = 1 and status = 1 and id = '${req.query.id}'`;

    try{
        const [vendor] = await sequelize.query(query, {
            type: QueryTypes.SELECT,
        });

        const reviews =
        await Review.findAll(
        { 
            where: { 
                reviewee: req.query.id,
                type: 'user'
            }, 
        });

        if (!vendor) { return apiResponse(res, 401, `Vendor not Found`); }
        vendor.reviews = reviews;
        return apiResponse(res, 200, `Success`,{vendor});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `There seems to be a problem Fetching your Menu Item. Please Try Again Later`);
    };
}

module.exports = {
    search,
    detail
};