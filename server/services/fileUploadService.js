const aws = require('aws-sdk');
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const multerS3 = require('multer-s3');
dotenv.config({ path: path.resolve(__dirname, '../../.env') });
const apiResponse = require('../utils/apiResponse');


aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    region: process.env.AWS_BUCKET_REGION
})
const s3 = new aws.S3();

const upload = multer({
    dest: 'uploads/',
    storage:multerS3({
        bucket: process.env.S3_BUCKET_NAME,
        s3: s3,
        acl: "public-read",
        key: (req, file, cb)=>{
            console.log(file)
            let filename = new Date().getTime()+'-'+file.originalname;
            cb(null,filename);
        }
    })
})


const download = async (req, res)=>{
    console.log(req.query)
    let x = await s3.getObject({
        Bucket: process.env.S3_BUCKET_NAME,
        Key: req.query.filename
    }).promise();
    res.send(x.Body);
}

module.exports = {upload, download}
