const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();
const db = require('../models');
const { KtUser } = db;
const mailerService = require('./mailerService')
const otp = require('../utils/otp');
const { Op } = require('sequelize');
const bcrypt = require('bcrypt');
const UserNotFoundException = require('../exceptions/UserNotFoundExeption');
const UserInactiveException = require('../exceptions/UserInactiveException');
const UserAuthenticationException = require('../exceptions/UserAuthenticationException');
const { validationResult } = require('express-validator');
const apiResponse = require('../utils/apiResponse');

async function login({ email, password }) {
    console.log("login1");
    const LoginUser = KtUser.scope('login');
        const [users] =
        await LoginUser.findAll(
        { 
            where: { 
                email: email,
            }, 
        })
        console.log("login2");

        if (!users) { throw new UserNotFoundException(`User Not Found`); }

        if (users.dataValues.status == '0' || users.dataValues.isVerified == '0') { 
            throw new UserInactiveException(`User Account has not been vefiried or is InActive`);
        }

        const matchHashedPassword = await bcrypt.compare(password, users.dataValues.password);
        if (!matchHashedPassword) { throw new UserAuthenticationException(`User credentials do not Match`); }

        const authtoken = jwt.sign({ id: users.dataValues.id, email: users.dataValues.email, role:  users.dataValues.type}, process.env.JWT_SECRET, { expiresIn: 600 });
        const auth = {
            authtoken: authtoken
        }
        return auth;    
}

async function verifyUser(req, res) {
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }
    const LoginUser = KtUser.scope('login');
    var now = new Date().toLocaleString();
    try{
        const [users] =
        await LoginUser.findAll(
        { 
            where: { 
                email: req.body.email,  
                otp: req.body.otp,
                otpIsUsed: '0',
                otpExpireAt: {
                    [Op.gt]: now,
                },
            }, 
        })
        if (!users) { return apiResponse(res, 401, `Authentication Fail`); }

        await KtUser.update(
            { 
                otpIsUsed: 1,
                isVerified: 1,
                status: 1,
            },
            { 
                where: { 
                    email: req.body.email
                }, 
            }
        )
        const authtoken = jwt.sign({ id: users.dataValues.id, email: users.dataValues.email, role:  users.dataValues.type }, process.env.JWT_SECRET, { expiresIn: '1d' });
        return apiResponse(res, 200, `Authentication Success`,{authtoken});
    }
    catch(e){
        console.log(e);
        return apiResponse(res, 500, `Server appears to be having a hard time, please try again later`);
    };
    
}

async function refreshOtp(req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return apiResponse(res, 400, errors);
    }
    const newotp = await otp.generate();

    try{
        await KtUser.update(
            {
                otp: newotp.otp,
                otpIsUsed: 0,
                otpExpireAt: newotp.expire,
            },
            { 
                where: { 
                    email: req.query.email
                }, 
            }
        )

        mailerService.send(
            req.query.email, 
            'Your Registration @ Kitchen', 
            `<html>
            <h2 style="color: #2e6c80;">
                Hi we are excited to&nbsp; have you <img src="https://html-online.com/editorfiles/tiny/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" />!</h2>
                <p>You have just few steps to complete your registration at Kitchen.&nbsp;</p>
                <p>Click to <a href="https://www.google.com.ng/" target="_blank"> <span style="background-color: #2b2301; color: #fff; display: inline-block; padding: 3px 10px; font-weight: bold; border-radius: 5px;">Here</span></a> to verify your account</p>
                <p><h3><strong><span style="color: #000080;">Use OTP: ${newotp.otp}</span></strong></h3>(*****Expires in 10 Minutes******)</p>
            </h2>
            </html>`
        );
        return apiResponse(res, 200, `If a user exists you'll receive an email`);
    }
    catch(e){
        console.log(`Err1: ${e.original}`);
        console.log(`Err2: ${e.parent}`);
        console.log(`Err3: ${e.errors}`);
        console.log(`Err3: ${e}`);
        return apiResponse(res, 500, e.errors[0].message);
    };
    
}

async function authorize(req, res, allowedRoles) {

    jwt.verify(req.headers.authtoken, process.env.JWT_SECRET, async function(err, decoded) {
        if(err || !allowedRoles.includes(decoded.role)){
            return apiResponse(res, 403, `Authorization Fail`);
        }else{
            return;
        }
    });
}

module.exports = {
    login,
    authorize,
    verifyUser,
    refreshOtp
};