'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Review extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Review.init({
    reviewer: DataTypes.STRING,
    reviewee: DataTypes.STRING,
    type: DataTypes.STRING,
    stars: DataTypes.STRING,
    comment: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'KtReview',
    defaultScope: {
      attributes: {
        exclude: ['id','reviewee','type','createdAt','updatedAt']
      },
      order: [['id', 'ASC']]
    }
  });
  return Review;
};