'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MenuItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  MenuItem.init({
    user: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    cost: DataTypes.STRING,
    quantityDefinition: DataTypes.STRING,
    quantityDefinitionCount: DataTypes.STRING,
    foodCategory: DataTypes.STRING,
    rating: DataTypes.STRING,
    imageName: DataTypes.STRING,
    imageUrl: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'KtMenuItem',
    defaultScope: {
      attributes: {
        exclude: ['user','createdAt','updatedAt']
      },
      order: [['id', 'ASC']]
    }
  });
  return MenuItem;
};