'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    addressLine1: DataTypes.STRING,
    addressLine2: DataTypes.STRING,
    addressLine3: DataTypes.STRING,
    country: DataTypes.STRING,
    isVerified: DataTypes.STRING,
    status: DataTypes.STRING,
    rating: DataTypes.STRING,
    type: DataTypes.STRING,
    otp: DataTypes.STRING,
    otpIsUsed: DataTypes.STRING,
    otpExpireAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'KtUser',
    defaultScope: {
      attributes: {
        exclude: ['password','id','isVerified','status','type','otp','otpIsUsed','otpExpireAt','createdAt','updatedAt']
      },
      order: [['id', 'ASC']]
    },
    scopes: {
      login: {
        attributes: {
          exclude: ['otp','otpIsUsed','otpExpireAt','createdAt','updatedAt']
        },
      }
    },
  });
  return User;
};