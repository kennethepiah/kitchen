
const generate = async () => {
    let digits = '0123456789'; 
    let otp = ''; 
    let len = digits.length 
    for (let i = 0; i < 6; i++) { 
        otp += digits[Math.floor(Math.random() * len)]; 
    } 
    var expire = new Date(new Date().getTime() + 10 * 60000).toLocaleString();

    return {otp, expire};
}

module.exports = {generate};