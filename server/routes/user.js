const Router = require('express');

const { check, body, query, param, header } = require('express-validator');
const apiController = require('../controllers/apiController');
const tryCatch = require('../utils/tryCatch');
const Auth = require('../middlewares/auth');
const user = new Router();

// ---- Schemas ----
/**
 * @swagger
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required:
 *              - firstName
 *              - email
 *              - password
 *              - country
 *              - type
 *          properties:
 *              id:
 *                  type: string
 *                  description: The auto-generated id of user
 *              firstName:
 *                  type: string
 *                  description: The First name of user
 *              lastName:
 *                  type: string
 *                  description: The Last name of user
 *              phone:
 *                  type: string
 *                  description: Phone number of user
 *              email:
 *                  type: string
 *                  description: Email ID of User
 *              password:
 *                  type: string
 *                  description: Unique password of ther user
 *              addressline1:
 *                  type: string
 *                  description: House No. and Street Name
 *              addressline2:
 *                  type: string
 *                  description: Town or City
 *              addressline3:
 *                  type: string
 *                  description: State
 *              country:
 *                  type: string
 *                  description: Country of user
 *              type:
 *                  type: string
 *                  description: User type (customer/ vendor)
 *          example:
 *              firstName: kenneth
 *              lastName: epiah
 *              phone: 08090000000
 *              email: kenneth.epiah@gmail.com
 *              password: Qwerty123456@
 *              addressLine1: 7 Allen Avenue
 *              addressLine2: Ikeja
 *              addressLine3: Lagos
 *              country: Nigeria
 * 
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      UserApiResponse:
 *          type: object
 *          required:
 *              - status
 *              - message
 *              - data
 *          properties:
 *              status:
 *                  type: string
 *                  description: The request status code returned by the server
 *              message:
 *                  type: string
 *                  description: The brief description about the status of request
 *              data:
 *                  type: string
 *                  description: The returned data about request
 *          example:
 *              status: 200
 *              message: success 
 *              data: 
 *                  $ref: '#/components/schemas/User'
 * 
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      UserAuthentication:
 *          type: object
 *          required:
 *              - email
 *              - password
 *          properties:
 *              email:
 *                  type: string
 *                  description: Email ID of User
 *              password:
 *                  type: string
 *                  description: Unique password of ther user
 *          example:
 *              email: kenneth.epiah@gmail.com
 *              password: Qwerty123456@
 * 
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      UserVerification:
 *          type: object
 *          required:
 *              - email
 *              - otp
 *          properties:
 *              email:
 *                  type: string
 *                  description: Email ID of User
 *              otp:
 *                  type: string
 *                  description: OTP code sent to user via email
 *          example:
 *              email: kenneth.epiah@gmail.com
 *              otp: 123456
*/

/**
 * @swagger
 * components:
 *  schemas:
 *      AuthenticationApiResponse:
 *          type: object
 *          required:
 *              - status
 *              - message
 *              - data
 *          properties:
 *              status:
 *                  type: string
 *                  description: The request status code returned by the server
 *              message:
 *                  type: string
 *                  description: The brief description about the status of request
 *              data:
 *                  type: string
 *                  description: The returned data about request
 *          example:
 *              status: 200
 *              message: success 
 *              data: 
 *                  authtoken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Imtlbm5ldGguby5lcGlhaEBnbWFpbC5jb20iLCJpYXQiOjE3MTk2MTIxOTUsImV4cCI6MTcxOTY5ODU5NX0.CBSu1G8GN-mEEHvU0es6geGfe6HvWfwRjwQ8wPv-LDQ"
 * 
 */


// ---- Tags ----
/**
 * @swagger
 * tags:
 *  name: Customer
 *  description: These APIs are available to Customers
 */

/**
 * @swagger
 * tags:
 *  name: Vendor
 *  description: These APIs are available to Vendors
 */

/**
 * @swagger
 * tags:
 *  name: Shared
 *  description: These APIs are available to both Customers and Vendors
 */

// ---- User Management ----

/**
 * @swagger
 *  /kitchen/user:
 *      post:
 *          summary: Creates a new Customer Account
 *          tags: [Customer]
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/User'
 * 
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/UserApiResponse'
 *              500:
 *                  description: Server Error
 */

/**
 * @swagger
 *  /kitchen/user:
 *      patch:
 *          summary: Update a Customer Account by id (email)
 *          tags: [Customer]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/User'
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/UserApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

/**
 * @swagger
 *  /kitchen/user/profile:
 *      get:
 *          summary: Returns the current Logged-In User profile
 *          tags: [Shared]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/UserApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

/**
 * @swagger
 *  /kitchen/user:
 *      delete:
 *          summary: Delete a Customer Account by id (email)
 *          tags: [Customer]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *          responses:
 *              200:
 *                  description: success
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

// ---- Vendor Management ----

/**
 * @swagger
 *  /kitchen/vendor/search:
 *      get:
 *          summary: Perform simple or complex search on the Vendors, Returns a List of Vendors
 *          tags: [Shared]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *              - in: query
 *                name: id
 *                schema:
 *                  type: string
 *                description: system auto generated id of the vendor
 *              - in: query
 *                name: firstName
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the vendor firstName (Business name) to search
 *              - in: query
 *                name: addressline1
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the vendor addressline1 (House No. and Street) to search
 *              - in: query
 *                name: addressline2
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the vendor addressline2 (Town or City) to search
 *              - in: query
 *                name: addressline3
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the vendor addressline3 (State) to search
 *              - in: query
 *                name: country
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the vendor country to search
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/UserApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

/**
 * @swagger
 *  /kitchen/vendor/detail:
 *      get:
 *          summary: Returns details of a Vendor
 *          tags: [Shared]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *              - in: query
 *                name: id
 *                schema:
 *                  type: string
 *                required: true
 *                description: system auto generated id of the vendor
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/UserApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */


// ---- Authentication ----

/**
 * @swagger
 *  /kitchen/user/login:
 *      post:
 *          summary: Authenticates user into the system
 *          tags: [Shared]
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/UserAuthentication'
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/AuthenticationApiResponse'
 *              500:
 *                  description: Server Error
 */

/**
 * @swagger
 *  /kitchen/user/verification:
 *      post:
 *          summary: Customer Account verification API
 *          tags: [Customer]
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/UserVerification'
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/AuthenticationApiResponse'
 *              500:
 *                  description: Server Error
 */

/**
 * @swagger
 *  /kitchen/user/verification:
 *      get:
 *          summary: Sends a fresh OTP code to Customers Email
 *          tags: [Customer]
 *          parameters:
 *              - in: query
 *                name: email
 *                schema:
 *                  type: string
 *                description: The Email ID of User
 *          responses:
 *              200:
 *                  description: success
 *              500:
 *                  description: Server Error
 */

//----------- User ------------------

user.post(
    '/kitchen/user', 
    [
        body('firstName', 'firstName feild must not be Empty').notEmpty().escape(),
        body('email', 'email feild must not be Empty').notEmpty().escape(),
        body('password', 'Password feild must not be Empty').notEmpty().escape(),
        body('country', 'Country feild must not be Empty').notEmpty().escape(),
        body('email', 'email must be valid').isEmail(),
        body('password', 'Password length should be 8 to 10 characters').isLength({ min: 8, max: 30 }),
        body('password', 'Password must contain at least eight characters, at least one number and both lower and uppercase letters and special characters').isStrongPassword()
    ],
    apiController.userCreate
);

user.get(
    '/kitchen/user/profile',
    Auth.hasRole(['customer','vendor']),
    tryCatch(apiController.userRead)
);

user.patch(
    '/kitchen/user',
    Auth.hasRole(['customer']),
    tryCatch(apiController.userUpdate)
);

user.delete(
    '/kitchen/user',
    Auth.hasRole(['customer']),
    tryCatch(apiController.userDestroy)
);


//----------- Vendor ------------------

user.get(
    '/kitchen/vendor/search',
    Auth.hasRole(['customer','vendor']),
    tryCatch(apiController.vendorListSearch)
);

user.get(
    '/kitchen/vendor/detail', 
    query('id', 'Email field must not be Empty').notEmpty().escape(),
    Auth.hasRole(['customer','vendor']),
    tryCatch(apiController.vendorDetailRead)
);

//----------- Auth ------------------

user.post(
    '/kitchen/user/verification', 
    body('otp', 'OTP feild must not be Empty').notEmpty().escape(),
    body('email', 'Email feild must not be Empty').notEmpty().escape(),
    body('email', 'Email must be valid').isEmail(),
    body('otp', 'OTP length should be 6 characters').isLength({ min: 6, max: 6 }),
    tryCatch(apiController.userVerify)
);

user.get(
    '/kitchen/user/verification', 
    query('email', 'Email feild must not be Empty').notEmpty().escape(),
    query('email', 'Email must be valid').isEmail(),
    tryCatch(apiController.userRefreshOtp)
);

user.post(
    '/kitchen/user/login', 
    body('email', 'email feild must not be Empty').notEmpty().escape(),
    body('password', 'password feild must not be Empty').notEmpty().escape(),
    tryCatch(apiController.userLogin)
);

module.exports = user;
