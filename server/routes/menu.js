const Router = require('express');
const { check, header, body, query, param } = require('express-validator');
const apiController = require('../controllers/apiController');
const tryCatch = require('../utils/tryCatch');
const Auth = require('../middlewares/auth');
const menu = new Router();
// const fs = require('../services/fileUploadService');

// ---- Schemas ----

/**
 * @swagger
 * components:
 *  schemas:
 *      MenuItem:
 *          type: object
 *          required:
 *              - name
 *              - description
 *              - cost
 *          properties:
 *              id:
 *                  type: string
 *                  description: The auto-generated id of menu-item
 *              name:
 *                  type: string
 *                  description: The name of this menu-item 
 *              description:
 *                  type: string
 *                  description: The brief description about menu-item, like ingredients, preparation and presentation
 *              cost:
 *                  type: string
 *                  description: The cost amount for menu-item in Naira
 *              quantityDefinition:
 *                  type: string
 *                  description: Describes how menu-item is served, plated or portioned when sold
 *              quantityDefinitionCount:
 *                  type: string
 *                  description: Unit count for the quantityDefinition
 *              foodCategory:
 *                  type: string
 *                  description: Categorize the menu-item to help customers find it more easilly
 *              imageUrl:
 *                  type: string
 *                  description: Pick a suitable Image for the menu-item to help customers appreciate your Offering
 *          example:
 *              name: Fufu and Afang Soup
 *              description: Fufu is soft and wrapped in 350gram balls. Freshly ground Afang Leaf with waterleaf, combined whith Palm Oild from the eastern Nigeria. 
 *              cost: 1500
 *              quantityDefinition: wrap(s)
 *              quantityDefinitionCount: 1
 *              foodCategory: swallow
 *              imageUrl: https://thedreamafrica.com/wp-content/uploads/2018/02/Afang-soup-recipe-1-e1560961354405.jpg
 * 
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      MenuApiResponse:
 *          type: object
 *          required:
 *              - status
 *              - message
 *              - data
 *          properties:
 *              status:
 *                  type: string
 *                  description: The request status code returned by the server
 *              message:
 *                  type: string
 *                  description: The brief description about the status of request
 *              data:
 *                  type: string
 *                  description: The returned data about request
 *          example:
 *              status: 200
 *              message: success 
 *              data: 
 *                  $ref: '#/components/schemas/MenuItem'
 * 
 */

// ---- Tags ----
/**
 * @swagger
 * tags:
 *  name: Customer
 *  description: These APIs are available to Customers
 */

/**
 * @swagger
 * tags:
 *  name: Vendor
 *  description: These APIs are available to Vendors
 */

/**
 * @swagger
 * tags:
 *  name: Shared
 *  description: These APIs are available to both Customers and Vendors
 */

// ---- Menu Item Management ----

/**
 * @swagger
 *  /kitchen/menu/item:
 *      post:
 *          summary: Creates a new menu-item
 *          tags: [Vendor]
 *          consumes:
 *              - multipart/form-data
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *          requestBody:
 *              content:
 *                  multipart/form-data:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              imageFile:
 *                                  type: file
 *                                  description: The file to upload
 *                                  required: true
 *                                  items:
 *                                      type: string
 *                                      format: binary
 *                              name:
 *                                  type: string
 *                                  description: The name of this menu item
 *                                  required: true
 *                              description:
 *                                  type: string
 *                                  description: A brief description about the menu-item, like ingredients, preparation or presentation
 *                                  required: true
 *                              cost:
 *                                  type: string
 *                                  description: The cost of this menu item
 *                                  required: true
 *                              quantityDefinition:
 *                                  type: string
 *                                  description: Measurement, portions or serving quantity (wraps, litres, killogram, paint rubber, etc)
 *                              quantityDefinitionCount:
 *                                  type: string
 *                                  description: The count for quantityDefinition feild (1, 2, 3, etc)
 *                              foodCategory:
 *                                  type: string
 *                                  description: The category of food that best describes this menu-item (protien, swallow, porridge etc)
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/MenuApiResponse'
 *              500:
 *                  description: Server Error
 *          x-swagger-router-controller: "Default"
 */

/**
 * @swagger
 *  /kitchen/menu/item:
 *      patch:
 *          summary: Update a menu-item by id
 *          tags: [Vendor]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *              - in: query
 *                name: id
 *                schema:
 *                  type: string
 *                required: true
 *                description: The unique auto-generated id of the menu-item
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/MenuItem'
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/MenuApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

/**
 * @swagger
 *  /kitchen/menu/item:
 *      get:
 *          summary: Returns the details of a menu-item
 *          tags: [Shared]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *              - in: query
 *                name: id
 *                schema:
 *                  type: string
 *                required: true
 *                description: The unique auto-generated id of the menu-item
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/MenuApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

/**
 * @swagger
 *  /kitchen/menu/item:
 *      delete:
 *          summary: Delete a menu-item by id
 *          tags: [Vendor]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *              - in: query
 *                name: id
 *                schema:
 *                  type: string
 *                required: true
 *                description: The unique auto-generated id of the menu-item
 *          responses:
 *              200:
 *                  description: success
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

/**
 * @swagger
 *  /kitchen/file:
 *      get:
 *          summary: Download a file
 *          tags: [Shared]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *              - in: query
 *                name: filename
 *                schema:
 *                  type: string
 *                required: true
 *                description: The name of file to download
 *          responses:
 *              200:
 *                  description: An image octet stream
 *                  content:
 *                      application/octet-stream:
 *                          schema:
 *                              type: string
 *                              format: binary
 * 
 */


// ---- Menu List Management ----

/**
 * @swagger
 *  /kitchen/menu/list:
 *      get:
 *          summary: Returns a List of menu items for current Logged-In Vendor.
 *          tags: [Vendor]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/MenuApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */

/**
 * @swagger
 *  /kitchen/menu/search:
 *      get:
 *          summary: Perform a simple or complex search on the Menu List. Returns a List of menu items.
 *          tags: [Shared]
 *          parameters:
 *              - in: header
 *                name: authtoken
 *                schema:
 *                  type: string
 *                required: true
 *                description: The authorization token for session management
 *              - in: query
 *                name: id
 *                schema:
 *                  type: string
 *                description: The unique auto-generated id of the menu-item
 *              - in: query
 *                name: user
 *                schema:
 *                  type: string
 *                description: The vendor id of the vendor who listed the menu-item
 *              - in: query
 *                name: name
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the menu-item name to search
 *              - in: query
 *                name: description
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the menu-item description to search
 *              - in: query
 *                name: costMinimum
 *                schema:
 *                  type: string
 *                description: The minimun cost for menu-item to search for
 *              - in: query
 *                name: costMaximum
 *                schema:
 *                  type: string
 *                description: The maximum cost for menu-item to search for
 *              - in: query
 *                name: foodCategory
 *                schema:
 *                  type: string
 *                description: Any string or fraction of the menu-item foodCategory to search
 *              - in: query
 *                name: createdAtMinimum
 *                schema:
 *                  type: string
 *                description: The minimun createdAt date menu-item to search for
 *              - in: query
 *                name: createdAtMaximum
 *                schema:
 *                  type: string
 *                description: The maximum createdAt date menu-item to search for
 *          responses:
 *              200:
 *                  description: success
 *                  content:
 *                      application/json:
 *                          schema:
 *                              items:
 *                                  $ref: '#/components/schemas/MenuApiResponse'
 *              404:
 *                  description: Item not Found
 *              500:
 *                  description: Server Error
 * 
 */


menu.post(
    '/kitchen/menu/item',
    // fs.upload.fields([
    //     { name: 'imageFile', maxCount: 1 }, 
    // ]), 
    //check('imageFile', 'file imageFile must not be Empty').notEmpty().escape(),
    body('name', 'name feild must not be Empty').notEmpty().escape(),
    body('description', 'description feild must not be Empty').notEmpty().escape(),
    body('cost', 'cost feild must not be Empty').notEmpty().escape(),
    Auth.hasRole(['vendor']),
    tryCatch(apiController.menuItemCreate)
);

menu.get(
    '/kitchen/menu/item', 
    query('id', 'id feild must not be Empty').notEmpty(),
    Auth.hasRole(['customer','vendor']),
    tryCatch(apiController.menuItemRead)
);

menu.patch(
    '/kitchen/menu/item',
    query('id', 'id feild must not be Empty').exists(),
    Auth.hasRole(['vendor']),
    tryCatch(apiController.menuItemUpate)
);

menu.delete(
    '/kitchen/menu/item', 
    query('id', 'id feild must not be Empty').exists(),
    Auth.hasRole(['vendor']),
    tryCatch(apiController.menuItemDestroy)
);

menu.get(
    '/kitchen/menu/list',
    Auth.hasRole(['vendor']),
    tryCatch(apiController.menuListRead)
);

menu.get(
    '/kitchen/menu/search',
    Auth.hasRole(['customer','vendor']),
    tryCatch(apiController.menuListSearch)
);

menu.get(
    '/kitchen/file',
    query('filename', 'name field must not be Empty').notEmpty(),
    Auth.hasRole(['customer','vendor']),
    tryCatch(apiController.fileDownload)
);

module.exports = menu;
