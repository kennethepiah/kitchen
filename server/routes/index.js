const express = require('express');
//const apiRoutes = require('./apiRoutes');
const user = require('./user');
const menu = require('./menu');

const router = express.Router();

//router.use('/api', apiRoutes);
router.use('/api', user);
router.use('/api', menu);

module.exports = router;
