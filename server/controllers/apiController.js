const authService = require('../services/authService.js');
const menuItemService = require('../services/menuItemService');
const menuListService = require('../services/menuListService');
const vendorService = require('../services/vendorService');
const userService = require('../services/userService');
//const fileService = require('../services/fileUploadService');
const { validationResult } = require('express-validator');
const apiResponse = require('../utils/apiResponse');
const UserNotFoundException = require('../exceptions/UserNotFoundExeption');
const UserInactiveException = require('../exceptions/UserInactiveException');
const UserAuthenticationException = require('../exceptions/UserAuthenticationException');
const HttpStatusCode = require('../enum/HttpStatusCode.js');

class apiController{

    // customer
    static async userLogin(req, res, next) {

        console.log("ksdsalkdslkdlk")
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse(res, HttpStatusCode.BadRequest, errors);
        }

        const { email, password } = req.body;
        try {
            const auth = await authService.login({ email, password })
            return apiResponse(res, HttpStatusCode.Ok, `Authentication Success`,auth);
        }
        catch(e){
            if(
                e instanceof UserNotFoundException
            ){
                return apiResponse(res, HttpStatusCode.NotFound, e.message);
            }
            if(
                e instanceof UserInactiveException || 
                e instanceof UserAuthenticationException
            ){
                return apiResponse(res, HttpStatusCode.Forbidden, e.message);
            }
            
            return apiResponse(res, HttpStatusCode.InternalServerError, e.message);
        }
    }

    static async userVerify(req, res, next) {
        authService.verifyUser(req, res)
            .then(user => user)
            .catch(next);
    }

    static async userRefreshOtp(req, res, next) {
        authService.refreshOtp(req, res)
            .then(user => user)
            .catch(next);
    }

    static async userCreate(req, res, next) {
        console.log("controller here")
        userService.create(req, res)
    }

    static async userRead(req, res, next) {
        userService.read(req, res)
    }

    static async userUpdate(req, res, next) {
        userService.update(req, res)
    }

    static async userDestroy(req, res, next) {
        userService.destroy(req, res)
    }

    static async vendorListSearch(req, res, next) {
        vendorService.search(req, res)
    }

    static async vendorDetailRead(req, res, next) {
        vendorService.detail(req, res)
    }

    static async menuItemCreate(req, res, next) {
        menuItemService.create(req, res)
    }

    static async menuItemRead(req, res, next) {
        menuItemService.read(req, res)
    }

    static async menuItemUpate(req, res, next) {
        menuItemService.update(req, res)
    }

    static async menuItemDestroy(req, res, next) {
        menuItemService.destroy(req, res)
    }

    static async menuListRead(req, res, next) {
        menuListService.read(req, res)
    }

    static async menuListSearch(req, res, next) {
        menuListService.search(req, res)
    }

    // static async fileDownload(req, res, next) {
    //     fileService.download(req, res)
    // }
}

module.exports = apiController;
