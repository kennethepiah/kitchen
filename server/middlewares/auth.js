const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const apiResponse = require('../utils/apiResponse');
dotenv.config();

class Auth {
    
    static hasRole (allowedRoles) {
        return async (req, res, next) => {
            jwt.verify(req.headers.authtoken, process.env.JWT_SECRET, async function(err, decoded) {
                if(err || !allowedRoles.includes(decoded.role)){
                    return apiResponse(res, 403, `Authorization Fail`);
                }
                next();
            });
        }
    }

}
module.exports = Auth;