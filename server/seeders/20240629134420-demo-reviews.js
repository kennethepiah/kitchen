'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * 
    */
    await queryInterface.bulkInsert('KtReviews', [
      {
        reviewer: '4',
        reviewee: '1',
        type: 'user',
        stars: '5',
        comment: 'I love this vendor. Every meal hits hard',
      },
      {
        reviewer: '4',
        reviewee: '1',
        type: 'user',
        stars: '5',
        comment: 'This vendor give me extra portions sometimes. I love it',
      },
      {
        reviewer: '4',
        reviewee: '2',
        type: 'user',
        stars: '2',
        comment: 'This vendors food tastes aweful, and always long time for food to be dispatched. Dont buy from here',
      },
      {
        reviewer: '4',
        reviewee: '1',
        type: 'menuitem',
        stars: '5',
        comment: 'I recommend if you love Rice and Stew when dey enter brain',
      },
      {
        reviewer: '4',
        reviewee: '1',
        type: 'menuitem',
        stars: '5',
        comment: 'Great food Taste',
      },
      {
        reviewer: '4',
        reviewee: '4',
        type: 'menuitem',
        stars: '5',
        comment: 'This is the best grilled fish i;ve had in some time. Great meal',
      },
      {
        reviewer: '4',
        reviewee: '5',
        type: 'menuitem',
        stars: '1',
        comment: 'More like stonny white rice and cury water. Do no buy this if you dont wann purge',
      },
      {
        reviewer: '4',
        reviewee: '6',
        type: 'menuitem',
        stars: '1',
        comment: 'Late delivery because it takes time to dispatch. Bad food taste',
      },
    ], {});
  }, 

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * 
     */
    await queryInterface.bulkDelete('KtReviews', null, {});
  }
};
