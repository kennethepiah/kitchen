'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     *
     */ 
    await queryInterface.bulkInsert('KtMenuItems', [
      {
        user: '1',
        name: 'Steamed White Rice and Chilli Stew',
        description: 'Lorem Ipsum',
        cost: '800',
        quantityDefinition: '1',
        quantityDefinitionCount: '1',
        foodCategory: '4',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '1',
        name: 'Boiled White Yam and Egg Omelette',
        description: 'Lorem Ipsum',
        cost: '800',
        quantityDefinition: '7',
        quantityDefinitionCount: '1',
        foodCategory: '4',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '1',
        name: 'Fried Chicken',
        description: 'Lorem Ipsum',
        cost: '800',
        quantityDefinition: '7',
        quantityDefinitionCount: '1',
        foodCategory: '5',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '1',
        name: 'Grilled Fish',
        description: 'Lorem Ipsum',
        cost: '800',
        quantityDefinition: '7',
        quantityDefinitionCount: '1',
        foodCategory: '5',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },

      //----------------------------------------

      {
        user: '2',
        name: 'Steamed White Rice and Curry Sauce',
        description: 'Lorem Ipsum',
        cost: '1200',
        quantityDefinition: '1',
        quantityDefinitionCount: '1',
        foodCategory: '4',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '2',
        name: 'Smookey Jolof Rice',
        description: 'Lorem Ipsum',
        cost: '1200',
        quantityDefinition: '1',
        quantityDefinitionCount: '1',
        foodCategory: '4',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '2',
        name: 'Fried Beef',
        description: 'Lorem Ipsum',
        cost: '1200',
        quantityDefinition: '7',
        quantityDefinitionCount: '1',
        foodCategory: '5',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '2',
        name: 'Grilled Chicken',
        description: 'Lorem Ipsum',
        cost: '1200',
        quantityDefinition: '7',
        quantityDefinitionCount: '1',
        foodCategory: '5',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },

      //----------------------------------------

      {
        user: '3',
        name: 'Steamed White Rice and Carrot Stew',
        description: 'Lorem Ipsum',
        cost: '1500',
        quantityDefinition: '1',
        quantityDefinitionCount: '1',
        foodCategory: '4',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '3',
        name: 'Fried Rice',
        description: 'Lorem Ipsum',
        cost: '1500',
        quantityDefinition: '1',
        quantityDefinitionCount: '1',
        foodCategory: '4',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '3',
        name: 'Fried Snail Meat',
        description: 'Lorem Ipsum',
        cost: '1500',
        quantityDefinition: '7',
        quantityDefinitionCount: '1',
        foodCategory: '5',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      },
      {
        user: '3',
        name: 'Grilled Turkey',
        description: 'Lorem Ipsum',
        cost: '1500',
        quantityDefinition: '7',
        quantityDefinitionCount: '1',
        foodCategory: '5',
        imageName: 'test.jpg',
        imageUrl: 'https://test.jpg'
      }

    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * 
     */
    await queryInterface.bulkDelete('KtMenuItems', null, {});
  }
};
