'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * 
    */
    await queryInterface.bulkInsert('KtQuantityDefinitions', [
      {
        user: '2',
        name: 'spoon serving'
      },
      {
        user: '2',
        name: 'cup serving'
      },
      {
        user: '2',
        name: 'basket'
      },
      {
        user: '2',
        name: 'paint container'
      },
      {
        user: '2',
        name: 'litre(s)'
      },
      {
        user: '2',
        name: 'kilogram(s)'
      },
      {
        user: '2',
        name: 'piece(s)'
      }
  ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * 
     */
    await queryInterface.bulkDelete('KtQuantityDefinitions', null, {});
  }
};
