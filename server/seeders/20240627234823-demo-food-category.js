'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * 
    */
    await queryInterface.bulkInsert('KtFoodCategories', [
      {
        user: '1',
        name: 'swallow',
        description: 'Eba, Pounded Yam, Amala, Fufu, Starch, Wheat etc'
      },
      {
        user: '1',
        name: 'soup',
        description: 'Egusi, Banga, Okoro, Edika-Ikong, Afang, Ewedu, Pepper Soup, White Soup etc'
      },
      {
        user: '1',
        name: 'porridge',
        description: 'Eba, Pounded Yam, Amala, Fufu, Starch, Wheat etc'
      },
      {
        user: '1',
        name: 'combo',
        description: 'Combine 2 or more Items into a single meal offering'
      },
      {
        user: '1',
        name: 'protien',
        description: 'Meat, Fish, Egg or Chicken Food garnishing or Sides'
      },
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * 
     */
    await queryInterface.bulkDelete('KtFoodCategories', null, {});
  }
};
