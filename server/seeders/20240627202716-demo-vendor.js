'use strict';

/** @type {import('sequelize-cli').Migration} */

const bcrypt = require('bcrypt');

 
module.exports = {
  async up (queryInterface, Sequelize) {
    
    const password = await bcrypt.hash("123456", 10);
    await queryInterface.bulkInsert('KtUsers', [
      {
        firstName: 'John',
        lastName: 'Doe',
        phone: '08034563737',
        email: 'kenneth.o.epiah@gmail.com',
        password: password,
        addressLine1: '5 Mobolaji Johnson Ave',
        addressLine2: 'Ikeja',
        addressLine3: 'Lagos',
        country: 'Nigeria',
        isVerified: '1',
        status: '1',
        type: 'vendor',
        otp: null,
        otpIsUsed: null,
        otpExpireAt: null
      },
      {
        firstName: 'Peggy',
        lastName: 'Jane',
        phone: '08096352727',
        email: 'biznezman.owhede@gmail.com',
        password: password,
        addressLine1: '5 Allen Ave',
        addressLine2: 'Ikeja',
        addressLine3: 'Lagos',
        country: 'Nigeria',
        isVerified: '1',
        status: '1',
        type: 'vendor',
        otp: null,
        otpIsUsed: null,
        otpExpireAt: null
      },
      {
        firstName: 'Richard',
        lastName: 'Alfredo',
        phone: '07086356534',
        email: 'biznexman@yahoo.com.com',
        password: password,
        addressLine1: '45 Bode Thomas Rd',
        addressLine2: 'Surulere',
        addressLine3: 'Lagos',
        country: 'Nigeria',
        isVerified: '1',
        status: '1',
        type: 'vendor',
        otp: null,
        otpIsUsed: null,
        otpExpireAt: null
      }
    ], {});
    
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('KtUsers', null, {});
  }
};
