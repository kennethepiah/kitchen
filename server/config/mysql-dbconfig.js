const dotenv = require('dotenv');
const bluebird = require('bluebird');

dotenv.config();

const mysql_params = {
    host: process.env.DB_HOST2,
    user: process.env.DB_USERNAME2,
    password: process.env.DB_PASSWORD2,
    database: process.env.DB_NAME2,
    Promise: bluebird
};

module.exports = mysql_params; 