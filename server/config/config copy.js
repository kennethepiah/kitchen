const dotenv = require('dotenv');
const path = require('path');
dotenv.config({ path: path.resolve(__dirname, '../../.env') });

const config = { 
  "development": {
    "username": process.env.DB_USERNAME2,
    "password": process.env.DB_PASSWORD2,
    "database": process.env.DB_NAME2,
    "host": process.env.DB_HOST2,
    "port": '3306',
    "dialect": "mysql",
    "dialectOptions": {
      //useUTC: false, // for reading from database
    },
    "timezone": '+01:00', // for writing to database
  },
  "test": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOST,
    "dialect": "mysql",
    "dialectOptions": {
      //useUTC: false, // for reading from database
    },
    "timezone": '+01:00', // for writing to database
  },
  "production": {
    "username": process.env.DB_USERNAME1,
    "password": process.env.DB_PASSWORD1,
    "database": process.env.DB_NAME1,
    "host": process.env.DB_HOST1,
    "dialect": "mysql",
    "dialectOptions": {
      //useUTC: false, // for reading from database
    },
    "timezone": '+01:00', // for writing to database
  }
}

module.exports = config;