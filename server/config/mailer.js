const nodemailer = require("nodemailer");
const path = require('path');
const dotenv = require('dotenv');
dotenv.config({ path: path.resolve(__dirname, '../../.env') });

const transporter = nodemailer.createTransport({
    service: "Gmail",
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: process.env.MAILER_APP_USER,
      pass: process.env.MAILER_APP_PASSWORD,
    },
});

module.exports = transporter;