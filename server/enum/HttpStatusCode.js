const HttpStatusCode = Object.freeze({
    Ok: 200,
    Created: 201,
    BadRequest: 400,
    Forbidden: 403,
    NotFound: 404,
    InternalServerError: 500,
})

module.exports = HttpStatusCode;
