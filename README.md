# kitchen

## Name
Kitchen API Restaurant Management Solution

# About this Project

RESTful API for a restaurant management product called Kitchen, the API has two (2) main consumers

- [ ] 1. Vendors: these are restaurants that use the platform to manage their menus 
- [ ] 2. Customers: these are customers who go on the platform to view the menus available across the restaurants

## Project status
This project is not in active Development/ Maintenance, but you may use it as is if it suits your purpose or you can [reach out](#support) for support if you have special requirements.

## Demo URL

[Demo](https://biznexmans-kitchen-app-9f1658018afc.herokuapp.com/documentation)

## Sample Vendor Login?

- You can create and demo the customer endpoints but if you want to try out the vendor endpoints just [reach out](#support), and we'll share you some sample credentials (At no cost to you)

## Support
Send an email to biznezman.owhede@gmail.com , with subject line "KitchenApp Support Request" so your communication is not lost in the mix

## Roadmap
Considering adding one or all of these depending on how much traction it gets in terms of support request
- [ ] Order Management
- [ ] Inventory Management

## Contributions and Usage
Clone, Fork, Enjoy!

## License
Permissive
